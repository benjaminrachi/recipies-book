import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { RecipeService } from 'shared/services/recipe.service';
import { RecipeFormComponent } from 'shared/components/recipe-form/recipe-form.component';

@NgModule({
  declarations: [
    RecipeFormComponent,
  ],
  providers: [
    RecipeService,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,

    RecipeFormComponent,
  ]
})
export class SharedModule { }
