import { RecipeService } from 'shared/services/recipe.service';
import { Component, OnInit, Input } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Recipe } from 'shared/models/recipe';

@Component({
  selector: 'recipe-form',
  templateUrl: './recipe-form.component.html',
  styleUrls: ['./recipe-form.component.scss'],
})
export class RecipeFormComponent implements OnInit {
  public recipeForm: FormGroup;
  @Input() recipe: Recipe;
  @Input() id: string;

  constructor(private formBuilder: FormBuilder, private recipeService: RecipeService) {}

  ngOnInit() {
    console.log('recipe-form inputs', this.recipe, this.id);

    this.recipeForm = this.formBuilder.group({
      name: this.formBuilder.control(this.recipe.name, [Validators.required]),
      duration: this.formBuilder.control(this.recipe.duration, []),
      ingredients: this.formBuilder.control(this.recipe.ingredients || [], []),
    });
   }

  get recipeControls() {
    return this.recipeForm.controls;
  }

  addIngredient(event) {
    this.recipeControls.ingredients.value.push(event.target.value);
    event.target.value = '';
  }

  removeIngredient(ingredient) {
    const index = this.recipeControls.ingredients.value.indexOf(ingredient);
    if (index > -1) {
      this.recipeControls.ingredients.value.splice(index, 1);
    }

  }

  save() {
    if (this.recipeForm.invalid) {
      return;
    }
    if (!this.id) {
      this.create();
      return;
    }
    this.update();
  }

  async create() {
    const res = await this.recipeService.create(this.recipeForm.value);
  }

  async update() {
    const res = await this.recipeService.update(this.id, this.recipeForm.value);
  }
}
