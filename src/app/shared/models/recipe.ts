export interface Recipe {
    id?: string;
    name: string;
    duration: number;
    ingredients: string[];
    imageUrl: string;
}
