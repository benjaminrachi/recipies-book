import { Recipe } from 'shared/models/recipe';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  recipesCollection: AngularFirestoreCollection<Recipe>;

  constructor(private afs: AngularFirestore) {
    this.recipesCollection = this.getRecipeCollection();
  }

  create(recipe: Recipe): Promise<DocumentReference> {
    return this.recipesCollection.add(recipe);
  }

  getRecipeCollection(): AngularFirestoreCollection<Recipe> {
    return this.afs.collection<Recipe>('recipes');
  }

  getAllRecipes(): Observable<Recipe []> {
    return this.recipesCollection.valueChanges({ idField: 'id' });
  }

  getRecipe(recipeId): Observable<Recipe> {
    return this.afs.doc<Recipe>(`recipes/${recipeId}`).valueChanges();
  }

  update(recipeId, newRecipe): Promise<void> {
    return this.afs.doc<Recipe>(`recipes/${recipeId}`).set(newRecipe, { merge: true });
  }

  delete(recipeId): Promise<void> {
    return this.afs.doc<Recipe>(`recipes/${recipeId}`).delete();
  }
}
