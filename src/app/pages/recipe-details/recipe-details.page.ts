import { RecipeService } from 'shared/services/recipe.service';
import { Recipe } from 'shared/models/recipe';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'recipe-details',
  templateUrl: './recipe-details.page.html',
  styleUrls: ['./recipe-details.page.scss'],
})
export class RecipeDetailsPage implements OnInit {
  id: string;
  recipe$: Observable<Recipe>;

  constructor(private route: ActivatedRoute, private recipeService: RecipeService) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');

    if (this.id) {
      this.recipe$ = this.recipeService.getRecipe(this.id);
    } else {
      this.recipe$ = of({} as Recipe);
    }
  }

}
