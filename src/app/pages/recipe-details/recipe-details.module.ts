import { SharedModule } from 'shared/shared.module';
import { NgModule } from '@angular/core';
import { RecipeDetailsPageRoutingModule } from './recipe-details-routing.module';

import { RecipeDetailsPage } from './recipe-details.page';


@NgModule({
  imports: [
    RecipeDetailsPageRoutingModule,
    SharedModule
  ],
  declarations: [RecipeDetailsPage]
})
export class RecipeDetailsPageModule {}
