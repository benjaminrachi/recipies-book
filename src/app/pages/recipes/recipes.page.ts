import { Recipe } from 'shared/models/recipe';
import { RecipeService } from 'shared/services/recipe.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'recipes',
  templateUrl: './recipes.page.html',
  styleUrls: ['./recipes.page.scss'],
})
export class RecipesPage implements OnInit {
  recipes$: Observable<Recipe[]>;
  constructor(private recipeService: RecipeService) { }

  ngOnInit() {
    this.recipes$ = this.recipeService.getAllRecipes();
  }

}
