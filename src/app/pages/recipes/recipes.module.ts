import { SharedModule } from 'shared/shared.module';
import { NgModule } from '@angular/core';
import { RecipesPageRoutingModule } from './recipes-routing.module';

import { RecipesPage } from './recipes.page';


@NgModule({
  imports: [
    SharedModule,
    RecipesPageRoutingModule,
  ],
  declarations: [RecipesPage]
})
export class RecipesPageModule {}
