export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyD1hVaLlIJZXbeHbiQyRyR44_s4zzFHWJ8',
    authDomain: 'recipes-book-d17bd.firebaseapp.com',
    databaseURL: 'https://recipes-book-d17bd.firebaseio.com',
    projectId: 'recipes-book-d17bd',
    storageBucket: 'recipes-book-d17bd.appspot.com',
    messagingSenderId: '557696053555',
    appId: '1:557696053555:web:2f43086b2122993a3d8e46',
    measurementId: 'G-TTXNC8K5RP'
  },
};
