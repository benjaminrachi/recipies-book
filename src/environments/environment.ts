// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyD1hVaLlIJZXbeHbiQyRyR44_s4zzFHWJ8',
    authDomain: 'recipes-book-d17bd.firebaseapp.com',
    databaseURL: 'https://recipes-book-d17bd.firebaseio.com',
    projectId: 'recipes-book-d17bd',
    storageBucket: 'recipes-book-d17bd.appspot.com',
    messagingSenderId: '557696053555',
    appId: '1:557696053555:web:2f43086b2122993a3d8e46',
    measurementId: 'G-TTXNC8K5RP'
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
